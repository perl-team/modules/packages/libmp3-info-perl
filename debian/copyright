Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://metacpan.org/release/MP3-Info
Upstream-Contact: J. J. Merelo-Guervós <jmerelo@cpan.org>
Upstream-Name: MP3-Info

Files: *
Copyright: 2017 JJ Merelo
           2006-2008 Dan Sully & Slim Devices, Inc.
           1998-2005 Chris Nandor.
License: Artistic or GPL-1+
License-Grant:
 All rights reserved. This program is free software; you can redistribute it
 and/or modify it under the same terms as Perl itself.

Files: inc/Module/Install*
Copyright: 2001-2006, Audrey Tang <autrijus@autrijus.org>
  2002-2004, Brian Ingerson <ingy@cpan.org>
  2006,2008-2012, Adam Kennedy <adamk@cpan.org>
License: Artistic or GPL-1+
Comment:
 Convenience copy of Module::Install.
 Not used during build.

Files: debian/*
Copyright: 1999-2000 Piotr Roszatycki <dexter@debian.org>
           2001-2002 Andras Bali <bali@debian.org>
           2004-2009 Alexander Wirt <formorer@debian.org>
           2021 Damyan Ivanov <dmn@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
